# Changelog

# 1.0.0

_Apr 19, 2019_

* Force announce list to be List
* Remove encoding field
* Release as 1.0.0 because it it quite stable

# 0.1.1

_Nov 6, 2017_

Update Symfony dependencies to allow Symfony 4

# 0.1.0

_Mar 30, 2017_

Initial release
Basic work with torrent files
