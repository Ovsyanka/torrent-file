# PHP Torrent File Library

[![Packagist](https://img.shields.io/packagist/v/sandfoxme/torrent-file.svg?maxAge=2592000)](https://packagist.org/packages/sandfoxme/torrent-file)
[![Packagist](https://img.shields.io/github/license/sandfoxme/torrent-file.svg?maxAge=2592000)](https://opensource.org/licenses/MIT)
[![Travis](https://img.shields.io/travis/sandfoxme/torrent-file.svg?maxAge=2592000)](https://travis-ci.org/sandfoxme/torrent-file)
[![Code Climate](https://img.shields.io/codeclimate/c/sandfoxme/torrent-file.svg?maxAge=2592000)](https://codeclimate.com/github/sandfoxme/torrent-file/coverage)
[![Code Climate](https://img.shields.io/codeclimate/maintainability/sandfoxme/torrent-file.svg?maxAge=2592000)](https://codeclimate.com/github/sandfoxme/torrent-file)

A PHP Class to work with torrent files

## Installation

Run ``composer require 'sandfoxme/torrent-file:^1.0'``

## Features

* Torrent file data manipulation
* Torrent file creation

## Documentation

Read full documentation here: <https://sandfox.dev/php/torrent-file.html>

## License

The library is available as open source under the terms of the [MIT License].

[MIT License]:  https://opensource.org/licenses/MIT
